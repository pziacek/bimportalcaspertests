// orsr form scrape example
var fs = require('fs'),
    system = require('system');

var casper = require('casper').create({
    verbose: false,
    logLevel: "debug"
});

casper.userAgent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');

var wholePage = '',
    f = null,
    pageFileName = 'page.html';

// get whole page as text
function getPage() {
	return document.documentElement.outerHTML;
}

function savePageToFile(fileName, content)
{
    // write page
	try {
	    fs.write(fileName, content, 'w');
	} catch(e) {
	    console.log(e);
	}
}

// get list of projectsList from page
var projectsList = [];
function getProjectNames() {
	var tags= document.querySelectorAll('div.project-box__name');
	return Array.prototype.map.call(tags, function(arg){
		return arg.innerHTML;
	});
}

// get log error info
var loginfo = [];
function getLogInfo() {
	var tags= document.querySelectorAll('div.AuthErrorStyle');
	return Array.prototype.map.call(tags, function(arg){
		return arg.innerHTML;
	});
}


// load page and fill form
casper.start('https://portal.bimplus.net/#/auth').waitForUrl(/auth/i, function fillForm() {

    // set a wait condition to make sure the page is loaded
    var loginName = 'pziacek@allplan.com';
    var pswd = 'bratislava1!';

    if( 0 < casper.cli.args.length )
    {
		loginName = casper.cli.get(0); 	// get first arg -> login name
        pswd = casper.cli.get(1);       // get second arg -> login password
    }
	else
		this.echo('No script arguments.' + 'Using default :' + loginName + ' ' + pswd);

    // fill out the form
    this.fillSelectors('form[data-ember-action"]', {'#inputloginname':loginName, '[name="password"]':pswd } , false);

    // click the login button
    this.echo("Logging In...");
    this.echo("");
    this.click('input[type="submit"][value="Log in"]');

    this.waitForUrl(/projects/i,function(){});
});

// get page as string and save it into file
casper.then(function () {
    this.echo('Getting list of projects ..');

    // write page to file
    // wholePage = this.evaluate(getPage);
    // savePageToFile(pageFileName, wholePage);

	// get and list projects
	projectsList = this.evaluate(getProjectNames);

    // print list of projects
    for (var i = 0; i < projectsList.length; ++i) {
		this.echo(projectsList[i]);
	}

    // check login info
	loginfo = this.evaluate(getLogInfo);

    // print login error information
	for (var i = 0; i < loginfo.length; ++i) {
	       this.echo(loginfo[i]);
	}

    this.wait(2000,function(){
        casper.viewport(1600, 1200);
        casper.capture('scr/prj.png')
    });
});

casper.run(function () {
	this.echo('');
	this.echo('Script done..');
	this.exit();
});
