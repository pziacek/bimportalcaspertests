// orsr form scrape example
var fs = require('fs'),
    system = require('system');

var casper = require('casper').create({
    verbose: false,
    logLevel: "debug"
});

var issueUrl = '';
casper.userAgent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');

var wholePage = '',
    f = null,
    pageFileName = 'page.html';

// get whole page as text
function getPage() {
	return document.documentElement.outerHTML;
}

function savePageToFile(fileName, content)
{
    // write page
	try {
	    fs.write(fileName, content, 'w');
	} catch(e) {
	    console.log(e);
	}
}

// get list of projectsList from page
var projectsList = [];
function getProjectNames() {
	var tags= document.querySelectorAll('div.project-box__name');
	return Array.prototype.map.call(tags, function(arg){
		return arg.innerHTML;
	});
}

// get log error info
var loginfo = [];
function getLogInfo() {
	var tags= document.querySelectorAll('div.AuthErrorStyle');
	return Array.prototype.map.call(tags, function(arg){
		return arg.innerHTML;
	});
}


// load page and fill form
casper.start('https://portal.bimplus.net/#/auth').waitForUrl(/auth/i, function fillForm() {

    // set a wait condition to make sure the page is loaded
    var loginName = 'pziacek@allplan.com';
    var pswd = 'bratislava1!';

    if( 0 < casper.cli.args.length )
    {
		loginName = casper.cli.get(0); 	// get first arg -> login name
        pswd = casper.cli.get(1);       // get second arg -> login password
    }
	else
		this.echo('No script arguments.' + 'Using default :' + loginName + ' ' + pswd);

    // fill out the form
    this.fillSelectors('form', {'#inputloginname':loginName, '[name="password"]':pswd } , false);

    // click the login button
    this.echo("Logging In...");
    this.echo("");
    this.click('input[type="submit"][value="Log in"]');

    this.waitForUrl(/projects/i,function(){});
});

// go to embedded issues list
// casper.then(function() {
//     this.echo("Selecting project");
//
//     this.wait(1000,function(){
//         this.evaluate( function() {
//             $('div:contains("My")').parents("li.project-box").find("div.project-box__preview ")[0].click();
//         });
//
//         this.wait(2000,function(){
//             issueUrl = this.getCurrentUrl();
//
//             issueUrl = issueUrl.replace('viewer','embeddedissuelist');
//             this.echo(issueUrl);
//
//             this.open(issueUrl);
//         });
//     });
// });


casper.thenOpen("https://portal.bimplus.net/#/embeddedissuelist?project_id=b1cd5f20-5cbc-4c91-98f2-b0d1acf05eb9&team_id=f370c815-0820-4dc4-931e-fe7e29c70c02",function() {
    this.echo("Listing issues");

    this.wait(2000,function(){
        this.echo(this.getCurrentUrl());

        // write page to file
        wholePage = this.evaluate(getPage);
        savePageToFile(pageFileName, wholePage);

        var list = this.evaluate( function() {
            return $('div.ember-table-body-container div.ember-table-table-row div.text:odd').text();
        });

        this.echo(list);
    });

});


casper.run(function () {
	this.echo('');
	this.echo('Script done..');
	this.exit();
});
